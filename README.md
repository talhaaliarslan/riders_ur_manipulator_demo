# Riders Ur Manipulator Demo

Riders platformunda UR5 robotu icin ilk kurulum ve ornek kullanim detaylari tasiyan bir paket.

## Demoyu baslatma

1. Editoru actiktan sonra `Edit Project Configuration` uzerinden asagidaki `.ride.yml` dosya taslagini kopyalayin

2. `Build Project` yaparak paketleri cekin. Catkin hatasi gelmesi normal, henuz **build** icin hazir degiliz

3. **Ctrl+`** ile terminal acip, 

```
cd /workspace/src/_lib/riders_ur_manipulator_demo/
```

ile pakate gidin ve `initial_setup.sh`'i calistirin

```
chmod +x initial_setup.sh
./initial_setup.sh
```

4. Calisma klasorune donup, projeyi insa edin

```
cd /workspace
catkin_make
```

5. Tekrar `Build Project` yapip projeyi kurun.

6. Simulasyonu `Start` ile baslatabilirsiniz.
 
7. **Ctrl+`** ile terminal acip, simulasyona yeni ur5 cagiralim.

```
roslaunch riders_ur5_bringup ur5_spawn.launch limited:=true
```

8. **Ctrl+Shift+`** ya da **Ctrl+Shift+5** ile ikinci bir terminal acin. Ilki de acik ve calisiyor kalsin.

Bu ikinci terminalde ur5_moveit calistiralim:

```
roslaunch ur5_moveit_config ur5_moveit_planning_execution.launch sim:=true limited:=true
```

9. **Ctrl+Shift+`** ya da **Ctrl+Shift+5** ile ucuncu bir terminal acin. Ilk ikisi de acik ve calisiyor kalsin.

Bu ucunu terminalde moveit_commander paketinin moveit_commander_cmdline programini cagiralim:

```
rosrun moveit_commander moveit_commander_cmdline.py 
```

Ornek komutlar:

```
 	use manipulator
 	ground
 	go up 0.1
```


## .ride.yml dosyasi taslak

`.ride.yml` :

```
requirements:
  packages:
    - 'https://github.com/ros-industrial/universal_robot'
    - 'https://github.com/tu-darmstadt-ros-pkg/hector_localization'
    - 'https://github.com/tu-darmstadt-ros-pkg/hector_gazebo'
    - 'https://github.com/tu-darmstadt-ros-pkg/hector_quadrotor'
    - 'https://gitlab.com/talhaaliarslan/riders-hector-demo'
    - 'https://gitlab.com/talhaaliarslan/riders_ur_manipulator_demo'
resources:
  robot_models:
    model1:
      package: hector_quadrotor_description
      urdf: urdf/quadrotor_with_cam.urdf.xacro
    model4:
      package: ur_description
      urdf: urdf/ur5_joint_limited_robot.urdf.xacro
  worlds:
    ur_world:
      package: riders_hector_worlds
      world_file: worlds/hoop_simple.world
runtime:
  robots:
    robot1:
      model: model1
      properties:
        Y: 0
        x: 0.5
        y: 1.15
        z: 0.5
  world: ur_world
```