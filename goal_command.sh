rostopic pub /arm_controller/follow_joint_trajectory/goal control_msgs/FollowJointTrajectoryActionGoal "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
goal_id:
  stamp:
    secs: 0
    nsecs: 0
  id: ''
goal:
  trajectory:
    header:
      seq: 0
      stamp:
        secs: 0
        nsecs: 0
      frame_id: 'world'
    joint_names: ['elbow_joint', 'shoulder_lift_joint', 'shoulder_pan_joint', 'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']
    points:
    -
      positions: [0, 0, 0, 0, 0, 0] 
      velocities: [0, 0, 0, 0, 0, 0]
      accelerations: [0, 0, 0, 0, 0, 0]
      time_from_start: {secs: 0, nsecs: 0}
    -
      positions: [0, 0, 1.57, 0, 0, 0] 
      velocities: [0, 0, 0, 0, 0, 0]
      accelerations: [0, 0, 0, 0, 0, 0]
      time_from_start: {secs: 5, nsecs: 0}
    -
      positions: [0, 0, 3.14, 0, 0, 0] 
      velocities: [0, 0, 0, 0, 0, 0]
      accelerations: [0, 0, 0, 0, 0, 0]
      time_from_start: {secs: 10, nsecs: 0}
    -
      positions: [0, 0, -1.57, 0, 0, 0] 
      velocities: [0, 0, 0, 0, 0, 0]
      accelerations: [0, 0, 0, 0, 0, 0]
      time_from_start: {secs: 15, nsecs: 0}
    -
      positions: [0, 0, 0, 0, 0, 0] 
      velocities: [0, 0, 0, 0, 0, 0]
      accelerations: [0, 0, 0, 0, 0, 0]
      time_from_start: {secs: 20, nsecs: 0}      
      "
    # -
    #   positions: [0, 0, 1.0, 0, 0, 0] 
    #   velocities: [0, 0, 0.5, 0, 0, 0]
    #   accelerations: [0, 0, 0, 0, 0, 0]
    #   time_from_start: {secs: 2, nsecs: 0}
    # -
    #   positions: [0, 0, 1.5, 0, 0, 0] 
    #   velocities: [0, 0, 0.5, 0, 0, 0]
    #   accelerations: [0, 0, 0, 0, 0, 0]
    #   time_from_start: {secs: 3, nsecs: 0}"